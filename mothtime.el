(require 'thingatpt)

(defun thing-at-point--end-of-time ()
  "Go to the end of the time at point."
  (let ((inhibit-changing-match-data t))
    (skip-chars-forward ":[:digit:]")
    (unless (looking-back "[[:digit:]]" nil)
      (error "No time here"))))

(defun thing-at-point--beginning-of-time ()
  "Go to the beginning of the time at point."
  (let ((inhibit-changing-match-data t))
    (skip-chars-backward ":[:digit:]")
    (unless (looking-at "[[:digit:]]")
      (error "No time here"))))

(defun thing-at-point--bounds-of-time-at-point ()
  "Get boundaries of time at point."
  (save-excursion
    (let (beg end)
      (thing-at-point--beginning-of-time)
      (setq beg (point))
      (thing-at-point--end-of-time)
      (setq end (point))
      (if (string-match-p "[[:digit:]]\\{1,2\\}:[[:digit:]]\\{2\\}[^[:digit:].*]?"
                          (buffer-substring-no-properties beg end))
          (cons beg end)
        (error "No time here")))))

(defun thing-at-point-time-at-point ()
  "Get time at point."
  (let ((bounds (bounds-of-thing-at-point 'time)))
    (mothtime--string-to-time (buffer-substring (car bounds) (cdr bounds)))))

(put 'time 'beginning-op 'thing-at-point--beginning-of-time)
(put 'time 'end-op 'thing-at-point--end-of-time)
(put 'time 'bounds-of-thing-at-point 'thing-at-point--bounds-of-time-at-point)
(put 'time 'thing-at-point 'thing-at-point-time-at-point)


(defun mothtime--operate-time-at-point (operation &optional arg)
  (if (not (listp (thing-at-point 'time)))
      (error "thing at point is not a time"))
  (let ((start-pos (point))
        (operand (or arg 1))
        (value (thing-at-point 'time)))
    (save-excursion
      (let ((bounds (bounds-of-thing-at-point 'time)))
        (kill-region (car bounds) (cdr bounds))
        (insert (format-time-string "%H:%M" (funcall operation value operand)))
        (pop kill-ring)))
    (goto-char start-pos)))

(defun mothtime--multiply-time-at-point (&optional arg)
  (interactive "p")
  (mothtime--operate-time-at-point '* arg))

(defun mothtime--divide-time-at-point (&optional arg)
  (interactive "p")
  (mothtime--operate-time-at-point '/ arg))

(defun mothtime--increment-hours-time-at-point (&optional arg)
  (interactive "p")
  (mothtime--operate-time-at-point 'mothtime--add-hours arg))

(defun mothtime--increment-minutes-time-at-point (&optional arg)
  (interactive "p")
  (mothtime--operate-time-at-point 'mothtime--add-minutes arg))

(defun mothtime--decrement-hours-time-at-point (&optional arg)
  (interactive "p")
  (let ((increment (or arg 1)))
    (mothtime--increment-hours-time-at-point (- 0 increment))))

(defun mothtime--decrement-minutes-time-at-point (&optional arg)
  (interactive "p")
  (let ((increment (or arg 1)))
    (mothtime--increment-minutes-time-at-point (- 0 increment))))

(defun mothtime--today ()
  (format-time-string "%Y-%m-%d" (date-to-time nil)))

(defun mothtime--string-to-time (time-string)
  (date-to-time (concat (mothtime--today) " " time-string)))

(defun mothtime--add-minutes (time minutes)
  (time-add time (* 60 minutes)))

(defun mothtime--add-hours (time hours)
  (time-add time (* 3600 hours)))

(provide 'mothtime)
